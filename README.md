# go-generics

A collection of useful algorithms/data structures implemented with generics.

## Disclaimer

There is probably some large organization that has implemented these same
concepts and actively maintains their library. You should use that instead of
this.
