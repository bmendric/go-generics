// list implements a doubly-linked list
//
// This is a generic re-implementation of container/list
//
// Copyright (c) 2009 The Go Authors
// Copyright (c) 2024 Brandon Mendrick
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//     Redistributions in binary form must reproduce the above copyright
//   - notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//   - Neither the name of Google Inc. nor the names of its contributors may
//     be used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
package list

type Element[T any] struct {
	Value T

	// The list that owns this this element
	list *List[T]

	// next and previous points in the doubly-linked list of elements. To
	// simplify the implementation, internally a list l is implemented as a ring,
	// such that &l.root is both the next element of the last list element and
	// the previous element of the first list element
	next     *Element[T]
	previous *Element[T]
}

// Previous returns the previous list element or nil
func (e *Element[T]) Previous() *Element[T] {
	if e.list == nil || e.previous == &e.list.root {
		return nil
	}

	return e.previous
}

// Next returns the next list element or nil
func (e *Element[T]) Next() *Element[T] {
	if e.list == nil || e.next == &e.list.root {
		return nil
	}

	return e.next
}

type List[T any] struct {
	root   Element[T]
	length int
}

// New returns an initialized list
func New[T any]() *List[T] {
	return new(List[T]).Init()
}

// Back returns the last element of the receiver or nil if the list is empty
func (l *List[T]) Back() *Element[T] {
	if l.length == 0 {
		return nil
	}

	return l.root.previous
}

// Front returns the first element of the receiver or nil if the list is empty
func (l *List[T]) Front() *Element[T] {
	if l.length == 0 {
		return nil
	}

	return l.root.next
}

// Init initializes or clears the receiver
func (l *List[T]) Init() *List[T] {
	l.root.previous = &l.root
	l.root.next = &l.root
	l.length = 0

	return l
}

func (l *List[T]) lazyInit() {
	if l.root.next == nil {
		l.Init()
	}
}

// insert inserts e after at, increments l.length, and return e
func (l *List[T]) insert(e, at *Element[T]) *Element[T] {
	e.previous = at
	e.next = at.next
	e.previous.next = e
	e.next.previous = e
	e.list = l
	l.length++

	return e
}

// remove removes e from the receiver, decrements l.length
func (l *List[T]) remove(e *Element[T]) {
	e.previous.next = e.next
	e.next.previous = e.previous

	// prevent memory leaks
	e.next = nil
	e.previous = nil
	e.list = nil

	l.length--
}

// move moves e to immediately after at
func (l *List[T]) move(e, at *Element[T]) {
	if e == at {
		return
	}

	e.previous.next = e.next
	e.next.previous = e.previous

	e.next = at.next
	e.previous = at
	e.next.previous = e
	e.previous.next = e
}

// InsertAfter inserts a new element, with value v, immediately after mark and
// returns the new element. If mark is not an element of the receiver, the list
// is not modified. The mark must not be nil.
func (l *List[T]) InsertAfter(v T, mark *Element[T]) *Element[T] {
	if mark.list != l {
		return nil
	}

	return l.insert(&Element[T]{Value: v}, mark)
}

// InsertBefore inserts a new element, with value v, immediately before mark
// and returns the new element. If mark is not an element of the receiver, the
// list is not modified. The mark must not be nil.
func (l *List[T]) InsertBefore(v T, mark *Element[T]) *Element[T] {
	if mark.list != l {
		return nil
	}

	return l.insert(&Element[T]{Value: v}, mark.previous)
}

// Len returns the number of elements of the receiver
//
// Complexity: O(1)
func (l *List[T]) Len() int {
	return l.length
}

// MoveAfter moves element e a new position immediately after mark. If e or
// mark is not an element of the receiver, or if e == mark, the list is not
// modified. e and mark must not be nil
func (l *List[T]) MoveAfter(e *Element[T], mark *Element[T]) {
	if e.list != l || e == mark || mark.list != l {
		return
	}

	l.move(e, mark)
}

// MoveBefore moves element e a new position immediately before mark. If e or
// mark is not an element of the receiver, or if e == mark, the list is not
// modified. e and mark must not be nil
func (l *List[T]) MoveBefore(e *Element[T], mark *Element[T]) {
	if e.list != l || e == mark || mark.list != l {
		return
	}

	l.move(e, mark.previous)
}

// MoveToBack moves element e to the back of the receiver. If e is not an
// element of the receiver, the list is not modified. e must not be nil
func (l *List[T]) MoveToBack(e *Element[T]) {
	if e.list != l || l.root.previous == e {
		return
	}

	l.move(e, l.root.previous)
}

// MoveToBack moves element e to the front of the receiver. If e is not an
// element of the receiver, the list is not modified. e must not be nil
func (l *List[T]) MoveToFront(e *Element[T]) {
	if e.list != l || l.root.next == e {
		return
	}

	l.move(e, &l.root)
}

// PushBack inserts a new element, with value v, at the back of the receiver
// and returns the new element.
func (l *List[T]) PushBack(v T) *Element[T] {
	l.lazyInit()

	return l.insert(&Element[T]{Value: v}, l.root.previous)
}

// PushBackList inserts a copy of another list at the back of the receiver. The
// receiver and other list may be the same. receiver and other must not be nil.
func (l *List[T]) PushBackList(other *List[T]) {
	l.lazyInit()
	for e := other.Front(); e != nil; e = e.next {
		l.insert(&Element[T]{Value: e.Value}, l.root.previous)
	}
}

// PushBack inserts a new element, with value v, at the front of the receiver
// and returns the new element.
func (l *List[T]) PushFront(v T) *Element[T] {
	l.lazyInit()

	return l.insert(&Element[T]{Value: v}, &l.root)
}

// PushBackList inserts a copy of another list at the front of the receiver. The
// receiver and other list may be the same. receiver and other must not be nil.
func (l *List[T]) PushFrontList(other *List[T]) {
	l.lazyInit()
	for e := other.Back(); e != nil; e = e.previous {
		l.insert(&Element[T]{Value: e.Value}, &l.root)
	}
}

// Remove removes element e from the receiver. If e is not an element of the
// receiver, the list is not modified. The value held by e is returned. e must
// not be nil.
func (l *List[T]) Remove(e *Element[T]) T {
	if e.list == l {
		l.remove(e)
	}

	return e.Value
}
