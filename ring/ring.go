// ring implements operations on circular lists
//
// This is a generic re-implementation of container/ring
//
// Copyright (c) 2009 The Go Authors
// Copyright (c) 2024 Brandon Mendrick
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//     Redistributions in binary form must reproduce the above copyright
//   - notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//   - Neither the name of Google Inc. nor the names of its contributors may
//     be used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
package ring

// Ring is an element of a circular list, or ring. Rings do not have a
// beginning or end; a pointer to any ring element serves as reference to the
// entire ring. Empty rings are represented as nil Ring pointers. The zero
// value for a Ring is a one-element ring with a nil value.
type Ring[T any] struct {
	Value T

	next     *Ring[T]
	previous *Ring[T]
}

// New creates a new ring of n elements
func New[T any](n int) *Ring[T] {
	if n <= 0 {
		return nil
	}

	r := new(Ring[T])
	p := r
	for i := 1; i < n; i++ {
		p.next = &Ring[T]{previous: p}
		p = p.next
	}
	p.next = r
	r.previous = p

	return r
}

func NewFromElements[T any](elements ...T) *Ring[T] {
	if len(elements) == 0 {
		return nil
	}

	r := &Ring[T]{Value: elements[0]}
	p := r
	for _, e := range elements[1:] {
		p.next = &Ring[T]{previous: p, Value: e}
		p = p.next
	}
	p.next = r
	r.previous = p

	return r
}

func (r *Ring[T]) init() *Ring[T] {
	r.next = r
	r.previous = r

	return r
}

// Do calls function f on each element of the ring, in forward order. The
// behavior of Do is undefined if f changes the receiver
func (r *Ring[T]) Do(f func(T)) {
	if r != nil {
		f(r.Value)
		for p := r.Next(); p != r; p = p.next {
			f(p.Value)
		}
	}
}

// Len computes the number of elements in the receiver
//
// Complexity: O(n)
func (r *Ring[T]) Len() int {
	n := 0
	if r != nil {
		n = 1
		for p := r.Next(); p != r; p = p.next {
			n++
		}
	}

	return n
}

// Link connects receiver ring r with ring s such that r.Next() becomes s and
// returns the original value for r.Next(). r must not be empty.
//
// If r and s point to the same ring, linking them removes the elements between
// r and s from the ring. The removed elements form a sub-ring and the result
// is a reference to that sub-ring (if no elements were removed, the results is
// still the original value of r.Next(), and not nil).
//
// If r and s point to different rings, linking them creates a single ring with
// the elements of s inserted after r. The result points to the element
// following the last element of s after insertion.
func (r *Ring[T]) Link(s *Ring[T]) *Ring[T] {
	n := r.Next()
	if s != nil {
		p := s.Previous()

		r.next = s
		s.previous = r
		n.previous = p
		p.next = n
	}

	return n
}

// Move moves n mod r.Len() elements backward (n < 0) or forward (n >= 0) in
// the receiver and returns that ring element. r must not be empty
func (r *Ring[T]) Move(n int) *Ring[T] {
	if r.next == nil {
		return r.init()
	}

	switch {
	case n < 0:
		for ; n < 0; n++ {
			r = r.previous
		}
	case n > 0:
		for ; n > 0; n-- {
			r = r.next
		}
	}

	return r
}

// Next returns the next ring element. r must not be empty
func (r *Ring[T]) Next() *Ring[T] {
	if r.next == nil {
		return r.init()
	}

	return r.next
}

// Previous returns the previous ring element. r must not be empty
func (r *Ring[T]) Previous() *Ring[T] {
	if r.next == nil {
		return r.init()
	}

	return r.previous
}

// Unlink remotes n mod r.Len() elements from the receiver ring, starting at
// r.Next(). If n mod r.Len() == 0, the receiver remains unchanged. The result
// is the removed sub-ring. r must not be empty
func (r *Ring[T]) Unlink(n int) *Ring[T] {
	if n <= 0 {
		return nil
	}

	return r.Link(r.Move(n + 1))
}
